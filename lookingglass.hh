#ifndef LOOKINGGLASS_HH
#define LOOKINGGLASS_HH

#include <TGFrame.h>

class TGLabel;
class TGNumberEntryField;
class TGeoManager;
class TGeoMaterial;
class TGeoMedium;
class TGeoVolume;
class TRootEmbeddedCanvas;
class TTree;

class lookingglass: public TGMainFrame {
  public:
    lookingglass(char const *);
    void Advance(int);
    void Next();
    void Plot();
    void Previous();
    void Refresh();
    void SetText();

  protected:
    ClassDef(lookingglass, 0);

  private:
    TRootEmbeddedCanvas *m_ecan;
    TGeoManager *m_geom;
    TTree *m_nl;
    int m_entry_i;
    TGNumberEntryField *m_entry;
    TGLabel *m_label;

    Double_t t0;
    Int_t nl_nbar;
    Double_t nl_x[400];
    Double_t nl_y[400];
    Double_t nl_z[400];
    Bool_t nl_fired[400];
    Int_t nb_nbar;
    Double_t nb_x[400];
    Double_t nb_y[400];
    Double_t nb_z[400];
    Bool_t nb_fired[400];
};

#endif
