MACRO:=lookingglass

CFLAGS:=-ggdb -I. $(shell root-config --cflags)
CPPFLAGS:=$(filter -I%,$(CFLAGS))
LIBS:=$(shell root-config --evelibs) $(shell pkg-config glew --libs)

.PHONY: all
all: $(MACRO)

$(MACRO): $(MACRO).o $(MACRO)_dict.o
	g++ -o $@ $^ $(LIBS)

$(MACRO)%.o: $(MACRO)%.cc Makefile
	g++ -c -o $@ $< $(CFLAGS)

$(MACRO)_dict.cc: $(MACRO).hh Makefile
	rootcint -f $@ -c $(CPPFLAGS) $<

.PHONY: clean
clean:
	rm -f *.o *_cc.* *_dict.* *.core $(MACRO)
