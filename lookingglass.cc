#include <TApplication.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGeoManager.h>
#include <TGeoMatrix.h>
#include <TGeoVolume.h>
#include <TRootEmbeddedCanvas.h>
#include <TTree.h>
#include <TView.h>
#include <iostream>
#include <sstream>
#include <lookingglass.hh>

namespace {

TApplication *g_app;

TGeoMatrix *WorldTrans(Double_t const a_x, Double_t const a_y, Double_t const
    a_z)
{
  TGeoRotation *r = new TGeoRotation;
  r->RotateX(90);
  return new TGeoCombiTrans(a_x, a_z, a_y, r);
}

}

lookingglass::lookingglass(char const *const a_path):
  TGMainFrame(gClient->GetRoot(), 10, 10, kHorizontalFrame),
  m_ecan(NULL),
  m_geom(NULL),
  m_nl(),
  m_entry_i(-1),
  m_entry(),
  m_label()
{
  TFile *file = new TFile(a_path);
  if (!file->IsOpen()) {
    std::cerr << a_path << ": Could not open.\n";
    exit(1);
  }
  m_nl = (TTree*)file->Get("nl");
  if (!m_nl) {
    std::cerr << a_path << ": No tree 'nl'.\n";
    exit(1);
  }

  m_nl->SetBranchAddress("t0", &t0);

  m_nl->SetBranchAddress("nl_nbar", &nl_nbar);
  m_nl->SetBranchAddress("nl_x", nl_x);
  m_nl->SetBranchAddress("nl_y", nl_y);
  m_nl->SetBranchAddress("nl_z", nl_z);
  m_nl->SetBranchAddress("nl_fired", nl_fired);

  m_nl->SetBranchAddress("nb_nbar", &nb_nbar);
  m_nl->SetBranchAddress("nb_x", nb_x);
  m_nl->SetBranchAddress("nb_y", nb_y);
  m_nl->SetBranchAddress("nb_z", nb_z);
  m_nl->SetBranchAddress("nb_fired", nb_fired);

  SetCleanup(kDeepCleanup);

  TGVerticalFrame *controls = new TGVerticalFrame(this);
  AddFrame(controls, new TGLayoutHints(kLHintsLeft | kLHintsExpandY));

  m_ecan = new TRootEmbeddedCanvas("LeCanvas", this, 500, 400);
  AddFrame(m_ecan, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGTextButton *prev = new TGTextButton(controls, "&Previous");
  prev->SetMargins(0, 0, 5, 5);
  prev->Connect("Clicked()", "lookingglass", this, "Previous()");
  controls->AddFrame(prev, new TGLayoutHints(kLHintsExpandX));

  TGTextButton *next = new TGTextButton(controls, "&Next");
  next->SetMargins(0, 0, 5, 5);
  next->Connect("Clicked()", "lookingglass", this, "Next()");
  controls->AddFrame(next, new TGLayoutHints(kLHintsExpandX));

  TGTextButton *refresh = new TGTextButton(controls, "&Refresh");
  refresh->SetMargins(0, 0, 5, 5);
  refresh->Connect("Clicked()", "lookingglass", this, "Refresh()");
  controls->AddFrame(refresh, new TGLayoutHints(kLHintsExpandX));

  TGTextButton *quit = new TGTextButton(controls, "&Quit");
  quit->SetMargins(0, 0, 5, 5);
  quit->Connect("Pressed()", "TApplication", g_app, "Terminate()");
  controls->AddFrame(quit, new TGLayoutHints(kLHintsExpandX));

  m_entry = new TGNumberEntryField(controls, -1, 0,
      TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative,
      TGNumberFormat::kNELLimitMinMax, 0, m_nl->GetEntries());
  m_entry->Connect("ReturnPressed()", "lookingglass", this, "SetText()");
  controls->AddFrame(m_entry, new TGLayoutHints(kLHintsExpandX, 0, 0, 20, 0));

  m_label = new TGLabel(controls);
  controls->AddFrame(m_label, new TGLayoutHints(kLHintsExpandX));

  Connect("CloseWindow()", "TApplication", g_app, "Terminate()");

  MapSubwindows();
  Resize();

  SetWindowName("Neutron trace viewer");
  MapRaised();

  Next();
}

void lookingglass::Advance(int const a_step)
{
  int prev_entry = m_entry_i;
  int nentries = m_nl->GetEntries();
  for (;;) {
    m_entry_i += a_step;
    if (0 > m_entry_i) {
      m_entry_i = nentries - 1;
    } else if (nentries <= m_entry_i) {
      m_entry_i = 0;
    }
    m_nl->GetEntry(m_entry_i);
    bool nl_yep = false;
    bool nb_yep = false;
    for (int i = 0; nl_nbar > i && !nl_yep; ++i) {
      if (nl_fired[i]) {
        nl_yep = true;
      }
    }
    for (int i = 0; nb_nbar > i && !nb_yep; ++i) {
      if (nb_fired[i]) {
        nb_yep = true;
      }
    }
    if (nl_yep && nb_yep) {
      Plot();
      return;
    }
    if (prev_entry == m_entry_i) {
      std::cerr << "No usable events in file.\n";
      exit(1);
    }
  }
}

void lookingglass::Next()
{
  Advance(1);
}

void lookingglass::Plot()
{
  if (m_geom) {
    delete m_geom;
  }

  m_geom = new TGeoManager("world", "Masters of the neutron");
  m_geom->SetNsegments(4);

  TGeoMaterial *vacuum = new TGeoMaterial("Vacuum", 0, 0, 0);
  TGeoMedium *air = new TGeoMedium("Air", 0, vacuum);

  TGeoMaterial *iron = new TGeoMaterial("Iron", 55.845, 26, 7.87);
  TGeoMedium *plastic = new TGeoMedium("Plastic", 1, iron);

  TGeoVolume *top = m_geom->MakeBox("Top", air, 1250, 1250, 400);
  m_geom->SetTopVolume(top);
  m_geom->SetTopVisible(0);

  TGeoVolume *neuland1 = m_geom->MakeBox("NeuLAND1", plastic, 1250, 1250, 25);
  TGeoVolume *neuland2 = m_geom->MakeBox("NeuLAND2", plastic, 1250, 1250, 25);
  TGeoVolume *neuland3 = m_geom->MakeBox("NeuLAND3", plastic, 1250, 1250, 25);
  TGeoVolume *neuland4 = m_geom->MakeBox("NeuLAND4", plastic, 1250, 1250, 25);
  TGeoVolume *neuland5 = m_geom->MakeBox("NeuLAND5", plastic, 1250, 1250, 25);
  TGeoVolume *neuland6 = m_geom->MakeBox("NeuLAND6", plastic, 1250, 1250, 25);
  TGeoVolume *neuland7 = m_geom->MakeBox("NeuLAND7", plastic, 1250, 1250, 25);
  TGeoVolume *neuland8 = m_geom->MakeBox("NeuLAND8", plastic, 1250, 1250, 25);

  TGeoVolume *nebula1 = m_geom->MakeBox("NEBULA1", plastic, 1800, 900, 60);
  TGeoVolume *nebula2 = m_geom->MakeBox("NEBULA2", plastic, 1800, 900, 60);
  TGeoVolume *nebula3 = m_geom->MakeBox("NEBULA3", plastic, 1800, 900, 60);
  TGeoVolume *nebula4 = m_geom->MakeBox("NEBULA4", plastic, 1800, 900, 60);

  // Coordinate system = (x, x, y).
  double const c_z_0 = 11000;

  top->AddNode(neuland1, 1, WorldTrans(0, 0, 11165 - c_z_0));
  top->AddNode(neuland1, 2, WorldTrans(0, 0, 11215 - c_z_0));
  top->AddNode(neuland1, 3, WorldTrans(0, 0, 11265 - c_z_0));
  top->AddNode(neuland1, 4, WorldTrans(0, 0, 11315 - c_z_0));
  top->AddNode(neuland1, 5, WorldTrans(0, 0, 11365 - c_z_0));
  top->AddNode(neuland1, 6, WorldTrans(0, 0, 11415 - c_z_0));
  top->AddNode(neuland1, 7, WorldTrans(0, 0, 11465 - c_z_0));
  top->AddNode(neuland1, 8, WorldTrans(0, 0, 11515 - c_z_0));

  top->AddNode(nebula1, 1, WorldTrans(0, 0, 13895.2 - c_z_0));
  top->AddNode(nebula2, 1, WorldTrans(0, 0, 14025.2 - c_z_0));
  top->AddNode(nebula3, 1, WorldTrans(0, 0, 14741.2 - c_z_0));
  top->AddNode(nebula4, 1, WorldTrans(0, 0, 14871.2 - c_z_0));

  for (int i = 0; nl_nbar > i; ++i) {
    if (!nl_fired[i]) {
      continue;
    }
    std::ostringstream oss;
    oss << "Point" << i;
    std::string name = oss.str();
    TGeoVolume *sphere = m_geom->MakeBox(name.c_str(), plastic, 25, 25, 25);
    oss.str("");
    oss.clear();
    oss << "rp" << i;
    name = oss.str();
    top->AddNode(sphere, 1, WorldTrans(nl_x[i], nl_y[i], nl_z[i] - c_z_0));
  }

  for (int i = 0; nb_nbar > i; ++i) {
    if (!nb_fired[i]) {
      continue;
    }
    std::ostringstream oss;
    oss << "Point" << i;
    std::string name = oss.str();
    TGeoVolume *sphere = m_geom->MakeBox(name.c_str(), plastic, 25, 25, 25);
    oss.str("");
    oss.clear();
    oss << "rp" << i;
    name = oss.str();
    top->AddNode(sphere, 1, WorldTrans(nb_x[i], nb_y[i], nb_z[i] - c_z_0));
  }

  m_geom->CloseGeometry();

  std::ostringstream oss;
  oss << m_entry_i;
  m_entry->SetText(oss.str().c_str());
  oss.clear();
  oss.str("");
  oss << "Max=" << m_nl->GetEntries();
  m_label->SetTitle(oss.str().c_str());

  TCanvas *canvas = m_ecan->GetCanvas();
  canvas->cd();
  m_geom->GetTopVolume()->Draw();
  gPad->GetView()->ShowAxis();
}

void lookingglass::Previous()
{
  Advance(-1);
}

void lookingglass::Refresh()
{
  Plot();
}

void lookingglass::SetText()
{
  std::istringstream iss(m_entry->GetText());
  iss >> m_entry_i;
  --m_entry_i;
  Next();
  std::ostringstream oss;
  oss << m_entry_i;
  m_entry->SetText(oss.str().c_str());
  Plot();
}

#ifndef __CINT__
int main(int argc, char **argv)
{
  if (2 != argc) {
    std::cerr << "Usage: " << argv[0] << " my_file.root\n";
    exit(1);
  }
  char const *path = argv[1];
  g_app = new TApplication("NeutronsBeware", &argc, argv);
  new lookingglass(path);
  g_app->Run();
  return 0;
}
#endif
